# Curso JavaScript Coderhouse

## Clase 03 - Ciclos / Iteraciones

### Crear un algoritmo utilizando un ciclo

---

**Tipo de desafío:** Complementario.

**Formato:** Página HTML y código fuente en JavaScript. Debe identificar el apellido del alumno/a en el nombre de archivo comprimido por "claseApellido".

**Sugerencia:** Usamos la instrucción **for** para repetir un número fijo de veces. Mientras que usamos **while** cuando queremos repetir algo hasta que se deje de cumplir una condición.

---

**>> Consigna:** 
Tomando como base los ejemplos anteriores de la estructura **for** y **while**, crear un algoritmo que repita un bloque de instrucciones. En cada repetición es necesario efectuar una operación o comparación para obtener una salida por alerta o consola.


**>> Aspectos a incluir en el entregable:**
Archivo HTML y Archivo JS, referenciado en el HTML por etiqueta `<script src="js/miarchivo.js"></script>`, que incluya la definición de un algoritmo en JavaScript que emplee bucles e instrucciones condicionales.

---

**>> Ejemplos:**
- Pedir número mediante `prompt()` y sumarle otro número en cada repetición, realizando una salida por cada resultado.
- Pedir un texto mediante `prompt()`, concatenar un valor en cada repetición, realizando una salida por cada resultado, hasta que se ingresa **"ESC"**.
- Pedir un número por `prompt()`, repetir la salida del mensaje **"Hola"** la cantidad de veces ingresada.
